var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/login', (req, res) => {
  res.render('login');
});

router.get('/admin', (req, res) => {
  res.render('admin');
});

router.get('/register', (req, res) => {
  console.log('Request for register page recieved');
  res.render('register');
});

module.exports = router;
