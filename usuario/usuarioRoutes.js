const express = require('express');
const router = express.Router();
const usuarioController = require('./usuarioController.js');
const auth = require('../routes/auth');

/*
 * GET
 */
router.get('/', usuarioController.list);

/*
 * GET
 */
router.get('/:id', usuarioController.show);

/*
 * POST
 */
router.post('/', usuarioController.create);

/*
 * PUT
 */
router.put('/:id', usuarioController.update);

/*
 * DELETE
 */
router.delete('/:id', usuarioController.remove);

module.exports = router;
