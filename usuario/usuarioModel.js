const mongoose = require('mongoose');
const Schema   = mongoose.Schema;
const crypto = require('crypto');
const jwt = require('jsonwebtoken');


const usuarioSchema = new Schema({
	'nome' : String,
	'email' : String,
	'papel' : String,
	'senha' : String
});


module.exports = mongoose.model('usuario', usuarioSchema);
