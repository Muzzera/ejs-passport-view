var usuarioModel = require('./usuarioModel.js');

/**
 * usuarioController.js
 *
 * @description :: Server-side logic for managing usuarios.
 */
module.exports = {

    /**
     * usuarioController.list()
     */
    list: function (req, res) {
        usuarioModel.find(function (err, usuarios) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting usuario.',
                    error: err
                });
            }
            return res.json(usuarios);
        });
    },

    /**
     * usuarioController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        usuarioModel.findOne({_id: id}, function (err, usuario) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting usuario.',
                    error: err
                });
            }
            if (!usuario) {
                return res.status(404).json({
                    message: 'No such usuario'
                });
            }
            return res.json(usuario);
        });
    },

    /**
     * usuarioController.create()
     */
    create: function (req, res) {
        var usuario = new usuarioModel({
			nome : req.body.nome,
			email : req.body.email,
			papel : req.body.papel,
			senha : req.body.senha

        });
        console.log(usuario);
        usuario.save(function (err, usuario) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating usuario',
                    error: err
                });
            }
            return res.status(201).json(usuario);
         });
    },

    /**
     * usuarioController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        usuarioModel.findOne({_id: id}, function (err, usuario) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting usuario',
                    error: err
                });
            }
            if (!usuario) {
                return res.status(404).json({
                    message: 'No such usuario'
                });
            }

            usuario.nome = req.body.nome ? req.body.nome : usuario.nome;
			usuario.email = req.body.email ? req.body.email : usuario.email;
			usuario.papel = req.body.papel ? req.body.papel : usuario.papel;
			usuario.senha = req.body.senha ? req.body.senha : usuario.senha;
			
            usuario.save(function (err, usuario) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating usuario.',
                        error: err
                    });
                }

                return res.json(usuario);
            });
        });
    },

    /**
     * usuarioController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        usuarioModel.findByIdAndRemove(id, function (err, usuario) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the usuario.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
